package com.whiz.prueba.service;

import com.whiz.prueba.controller.request.ClienteRequest;
import org.springframework.http.ResponseEntity;

public interface ClienteService {
    ResponseEntity<?> registrar(ClienteRequest request);
    ResponseEntity<?> consultar(Long id);
}
