package com.whiz.prueba.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.whiz.prueba.controller.request.ClienteRequest;
import com.whiz.prueba.controller.response.ExceptionError;
import com.whiz.prueba.controller.response.ResponseCliente;
import com.whiz.prueba.model.Cliente;
import com.whiz.prueba.repository.ClienteRepository;
import com.whiz.prueba.service.ClienteService;
import com.whiz.prueba.util.Constantes;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Date;
import java.util.Set;

@Slf4j
@Service
public class ClienteServiceImpl implements ClienteService {
    
    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private Constantes constantes;

    @Override
    public ResponseEntity<?> registrar(ClienteRequest request) {
        ResponseCliente response = new ResponseCliente();
        try{
            log.info("Inicio de Registro Cliente {}", mapper.writeValueAsString(request));
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            Validator validator = factory.getValidator();
            Set<ConstraintViolation<ClienteRequest>> constraintViolations = validator.validate(request);
            if (constraintViolations.iterator().hasNext()) {
                response.setMensaje(constraintViolations.iterator().next().getMessage());
                response.setStatus(HttpStatus.BAD_REQUEST);
                response.setCodigo(400);
                return new ResponseEntity<>(response, response.getStatus());
            }

            if(clienteRepository.findBynombre(request.getNombre()).isPresent()){
                response.setMensaje(constantes.EXNOMBRE);
                response.setStatus(HttpStatus.BAD_REQUEST);
                response.setCodigo(400);
                return new ResponseEntity<>(response, response.getStatus());
            }

            Cliente cliente = new Cliente();
            cliente.setNombre(request.getNombre());
            cliente.setApellidoMaterno(request.getApeMat());
            cliente.setApellidoPaterno(request.getApePat());
            cliente.setCorreo(request.getCorreo());
            Date fecha = constantes.dateFormat.parse(request.getFechaNac());
            cliente.setFechaNacimiento(fecha);
            cliente.setTelefono(request.getTelefono());
            cliente.setSexo(request.getSexo());

            var res = clienteRepository.save(cliente);

            response.setCodigo(201);
            response.setMensaje(constantes.REGISTROOK);
            response.setStatus(HttpStatus.CREATED);
            response.setClienteId(res.getId());
        }catch(Exception ex){
            ExceptionError error = new ExceptionError();
            error.setCodigo(HttpStatus.INTERNAL_SERVER_ERROR.value());
            error.setMensajes(ex.getLocalizedMessage());
            error.setDetalles(ex.getMessage());
            error.setTimestamp(new Date());

            log.error("Error ClienteServiceImpl.Registrar {}", error);

            return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, response.getStatus());
    }

    @Override
    public ResponseEntity<?> consultar(Long id) {
        ResponseCliente response = new ResponseCliente();
        try{
            log.info(constantes.CONSULTACLINETEINIT, mapper.writeValueAsString(id));
            if(clienteRepository.findById(id).isEmpty()){
                response.setMensaje(constantes.CLIENTENOEXISTE);
                response.setStatus(HttpStatus.BAD_REQUEST);
                response.setCodigo(400);
                return new ResponseEntity<>(response, response.getStatus());
            }
            var cliente = clienteRepository.findById(id).get();
            response.setCodigo(HttpStatus.OK.value());
            response.setMensaje(constantes.CONSULTACLIENTEOK);
            response.setStatus(HttpStatus.OK);
            response.setCliente(cliente);
        }catch(Exception ex){
            ExceptionError error = new ExceptionError();
            error.setCodigo(HttpStatus.INTERNAL_SERVER_ERROR.value());
            error.setMensajes(ex.getLocalizedMessage());
            error.setDetalles(ex.getMessage());
            error.setTimestamp(new Date());

            log.error("Error ClienteServiceImpl.Registrar {}", error);

            return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, response.getStatus());
    }
}
