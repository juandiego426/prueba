package com.whiz.prueba.controller.request;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.Data;

@Data
public class ClienteRequest {
    
    @NotBlank
    @NotNull
    @Pattern(regexp = "^[a-zA-Z ]{2,50}+$", message = "Ingrese nombre valido")
    @ApiModelProperty(position = 1, required = true, name = "nombre", dataType = "String", example = "Josselyn")
    private String nombre;
    
    @NotBlank
    @NotNull
    @Pattern(regexp = "^[a-zA-Z ]{2,50}+$", message = "Ingrese Apellido Paterno valido")
    @ApiModelProperty(position = 2, required = true, name = "apePat", dataType = "String", example = "Cosco")
    private String apePat;
    
    @NotBlank
    @NotNull
    @Pattern(regexp = "^[a-zA-Z ]{2,50}+$", message = "Ingrese Apellido Materno valido")
    @ApiModelProperty(position = 3, required = true, name = "apeMat", dataType = "String", example = "Hilario")
    private String apeMat;
    
    @NotBlank
    @NotNull
    @Pattern(regexp = "[1-9][0-9][0-9]{2}-([0][1-9]|[1][0-2])-([1-2][0-9]|[0][1-9]|[3][0-1])", message = "Ingrese Fecha nacimiento valido")
    @ApiModelProperty(position = 4, required = true, name = "fechaNac", dataType = "String", example = "1996-12-07")
    private String fechaNac;
    
    @NotBlank
    @NotNull
    @Pattern(regexp = "^[a-zA-Z ]{2,30}+$", message = "Ingrese sexo valido")
    @ApiModelProperty(position = 5, required = true, name = "sexo", dataType = "String", example = "Femenino")
    private String sexo;
    
    @NotBlank
    @NotNull
    @Email(message = "Ingrese correo valido")
    @ApiModelProperty(position = 6, required = true, name = "correo", dataType = "String", example = "jcosco@gmail.com")
    private String correo;
    
    @NotBlank
    @NotNull
    @Pattern(regexp = "^[0-9 ]{7,9}+$", message = "Ingrese nombre valido")
    @ApiModelProperty(position = 7, required = true, name = "telefono", dataType = "String", example = "963258741")
    private String telefono;
}
