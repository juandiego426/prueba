package com.whiz.prueba.controller;

import com.whiz.prueba.controller.request.ClienteRequest;
import com.whiz.prueba.controller.response.ExceptionError;
import com.whiz.prueba.controller.response.ResponseCliente;
import com.whiz.prueba.service.ClienteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/clientes")
@CrossOrigin("*")
@Api( tags = "Servicio Controlador Clientes", produces = "application/json", value = "Clientes")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @ApiOperation(value = "Registro Clientes", tags = "Servicio Controlador Clientes")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "OK", response = ResponseCliente.class),
            @ApiResponse(code = 400, message = "SOLICITUD INCORRECTA ", response = ResponseCliente.class),
            @ApiResponse(code = 404, message = "NO ENCONTRADO"),
            @ApiResponse(code = 500, message = "Error en el Servidor", response = ExceptionError.class)
    })
    @PostMapping(path = "/registro", consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> registro(@RequestBody ClienteRequest request){
        return clienteService.registrar(request);
    }


    @ApiOperation(value = "Consulta Clientes", tags = "Servicio Controlador Clientes")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ResponseCliente.class),
            @ApiResponse(code = 400, message = "SOLICITUD INCORRECTA ", response = ResponseCliente.class),
            @ApiResponse(code = 404, message = "NO ENCONTRADO"),
            @ApiResponse(code = 500, message = "Error en el Servidor", response = ExceptionError.class)
    })
    @GetMapping(path = "/{clienteId}", produces = "application/json")
    public ResponseEntity<?> consulta(@PathVariable(name = "clienteId") Long clienteId){
        return clienteService.consultar(clienteId);
    }
}
