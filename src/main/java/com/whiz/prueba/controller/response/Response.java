package com.whiz.prueba.controller.response;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class Response {
    
    private int codigo;
    private String mensaje;
    private HttpStatus status;
}
