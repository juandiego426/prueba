package com.whiz.prueba.controller.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.whiz.prueba.model.Cliente;
import lombok.Data;

@Data
public class ResponseCliente extends Response{

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private Long clienteId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Cliente cliente;
}
