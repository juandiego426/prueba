package com.whiz.prueba.controller.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import lombok.Data;

@Data
public class ExceptionError {
    
    private Date timestamp;
    private String mensajes;
    private String detalles;
    private Integer codigo;
}
