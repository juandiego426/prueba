package com.whiz.prueba.util;

import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;

@Component
public class Constantes {

    public static final String REGISTROOK = "Cliente Registrado Satisfactoriamente";
    public static final String CONSULTACLINETEINIT = "Cliente id a consultar {}";
    public static final String CONSULTACLIENTEOK = "Cliente encontrado";
    public static final String CLIENTENOEXISTE = "el id de cliente no existe";
    public SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public static final String EXNOMBRE = "Existe el nombre ingresado";
}
