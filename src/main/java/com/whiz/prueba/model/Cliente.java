package com.whiz.prueba.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

/**
 *
 * @author juan_
 */
@Data
@Entity
@Table
@NoArgsConstructor
public class Cliente implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, unique = true)
    private Long id;

    @Column(name = "nombre",length = 50, unique = true)
    private String nombre;

    @Column(name = "apellido_paterno",length = 50)
    private String apellidoPaterno;

    @Column(name = "apellido_materno", length = 50)
    private String apellidoMaterno;

    @Column(name = "fecha_nacimiento", columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaNacimiento;

    @Column(name = "sexo", length = 30)
    private String sexo;

    @Column(name = "correo", length = 150)
    private String correo;

    @Column(name = "telefono", length = 10)
    private String telefono;

    @Column(name = "fecha_registro", columnDefinition = "DATETIME", insertable = true, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    @JsonIgnore
    private Date fechaRegistro;

    @Column(name = "fecha_actualizacion", columnDefinition = "DATETIME", insertable = false, updatable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    @JsonIgnore
    private Date fechaActualizacion;

}
